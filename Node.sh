#!/bin/bash

# wget https://gitlab.com/figuerom16/misc/-/raw/main/Node.sh && chmod +x Node.sh && ./Node.sh

# Script Test
if [[ $(id -u) != 0 ]]; then
	echo 'Script must be run as super user.'
	echo 'name required and will be forced to lower and padding removed.'
	echo '# ./Node.sh'
	exit 0
fi

name=$(echo $1 | tr -d "\t\n\r")

#Install
apt update && apt dist-upgrade -y
apt install curl wget git -y
curl -fssL https://deb.nodesource.com/setup_20.x | bash -
curl -fsSL https://get.pnpm.io/install.sh | bash -
apt install nodejs -y

#Linux Environment Setup
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
reboot