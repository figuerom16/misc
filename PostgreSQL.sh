#!/bin/bash

#Update and Install Dependencies Example:
#apt update && apt dist-upgrade -y && apt install postgresql -y

if [[ $# -ne 2 || $(id -u) != 0 ]]; then
	echo 'Script must be run as super user.'
	echo 'name required and will be forced to lower and padding removed.'
	echo '# ./DebPostgreSQL.sh <name> <password>'
	exit 0
fi

name=$(echo $1 | tr -d "\t\n\r")

#Install/Setup PostgreSQL Seperate Server
runuser -l postgres -c psql << EOT
CREATE USER $name WITH PASSWORD '$2';
CREATE DATABASE $name OWNER $name;
ALTER ROLE $name SET client_encoding TO 'utf8';
ALTER ROLE $name SET default_transaction_isolation TO 'read committed';
ALTER ROLE $name SET timezone TO 'UTC';
GRANT ALL ON DATABASE $name TO $name;
\q
EOT
sed -i "s|#listen_addresses =.*|listen_addresses = '*' # what IP address(es) to listen on;|" /etc/postgresql/*/main/postgresql.conf
sed -i "s|127.0.0.1/32|0.0.0.0/0   |g" /etc/postgresql/*/main/pg_hba.conf

#Environment Settings
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc

#Cleanup
rm PostgreSQL.sh
reboot