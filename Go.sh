#!/bin/bash

#Update and Install Dependencies Example:
sed -i '/# en_US.UTF-8/s|^# ||' /etc/locale.gen && locale-gen
apt update && apt dist-upgrade -y && apt install build-essential curl wget git -y

#Check for root and arguments.
if [[ $# -lt 2 || $(id -u) != 0 ]]; then
	echo 'Script must be run as super user.'
	echo 'name required and will be forced to lower and padding removed.'
	echo 'go_version required'
	echo 'project_link is optional'
	echo '# ./Go.sh <name> <go_version> <project_link>'
	echo 'example # ./Go.sh api 1.22.1 github.com/username/project'
	exit 0
fi

name=$(echo $1 | tr -d "\t\n\r")
go_version=$(echo $2 | tr -d "\t\n\r")
project_link=$(echo $3 | tr -d "\t\n\r")

#Move into project folder
cd /opt

#Install Go
wget https://go.dev/dl/go$go_version.linux-amd64.tar.gz
tar -C /usr/local -xzf go$go_version.linux-amd64.tar.gz
rm go$go_version.linux-amd64.tar.gz

#Git Clone and build Go project
if [ -n "$project_link" ]; then
	export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
	git clone $project_link $name
	cd $name
	go build -o $name
fi

#Create systemd go service
cat << EOT > /etc/systemd/system/$name.service
[Unit]
Description=$name daemon
After=network.target

[Service]
User=root
Group=root
WorkingDirectory=/opt/$name
ExecStart=/opt/$name/$name

[Install]
WantedBy=multi-user.target
EOT
systemctl daemon-reload
systemctl enable $name

#Environment Settings
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc
echo 'export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin' >> ~/.bashrc
echo "cd /opt/$name" >> ~/.bashrc
reboot