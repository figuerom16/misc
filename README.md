# Misc
## Helper files. This will contain config files for Linux that are either for environment configuation or for pressing server stacks.

### Linux GoRedis
- Create folder under opt
- Install Go
- Git Clone and build Go project
- Create systemd go service
- Open Redis Unix Socket
- Set environment settings
- Cleanup

###### Debian Example:
```
apt update && apt dist-upgrade -y && apt install curl wget git redis -y
wget https://gitlab.com/figuerom16/misc/-/raw/main/GoRedis.sh && chmod +x GoRedis.sh
./GoRedis.sh api 1.20.4 https://gitlab.com/figuerom16/central-api.git
```

### Linux PostgreSQL
- Setup PostgeSQL user
- Open external ip interfaces
- Set environment settings
- Cleanup

###### Debian Example:
```
apt update && apt dist-upgrade -y && apt install postgresql -y
wget https://gitlab.com/figuerom16/misc/-/raw/main/PostgreSQL.sh && chmod +x PostgreSQL.sh
./PostgreSQL.sh api somepassword

```