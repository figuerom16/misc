#!/bin/bash
if [[ $# -ne 1 || $(id -u) != 0 ]]; then
	echo 'Script must be run as super user.'
	echo 'name required and will be forced to lower and padding removed.'
	echo '# ./DebUnitPython.sh <name>'
	exit 0
fi

name=$(echo $1 | tr -d "\t\n\r")

#Install
apt update && apt dist-upgrade -y
apt install curl wget python3-venv -y
wget https://raw.githubusercontent.com/nginx/unit/master/tools/setup-unit && chmod +x setup-unit
./setup-unit repo-config
apt install unit unit-python3.9
systemctl restart unit

#Project Folder and Python Initial Setup
mkdir /opt/$name && cd /opt/$name
python3 -m venv venv
source /opt/$name/venv/bin/activate
pip install -U pip setuptools wheel
deactivate
chown -R unit:unit /opt/$name

#Debian Environment Setup
echo "alias venv='source /opt/$name/venv/bin/activate'" >> ~/.bashrc
echo "alias fixperm='chown -R unit:unit /opt/$name'" >> ~/.bashrc
echo "source /opt/$name/venv/bin/activate" >> ~/.bashrc
echo "cd /opt/$name" >> ~/.bashrc
sed -i '/# export LS_OPTIONS/s|^# ||' ~/.bashrc
sed -i '/# eval "$(dircolors)"/s|^# ||' ~/.bashrc
sed -i "/# alias ls='ls /s|^# ||" ~/.bashrc